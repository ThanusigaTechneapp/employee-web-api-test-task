namespace EmployeeWebApi.Pagination
{
     public class MetaData
    {
        public int TotalPages { get; set; }
        
        public int TotalCount { get; set; }

        public int CurrentPage { get; set; }
    }
}