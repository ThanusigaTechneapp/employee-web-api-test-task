using System.ComponentModel.DataAnnotations;

namespace EmployeeWebApi.Pagination
{
    public class PaginatedQuery
    {
        public int Page { get; set; } = 1;

        public int PerPage { get; set; } = 5;

        public string Keyword { get; set; }

        [RegularExpression("asc|desc", ErrorMessage = "SortDirection should be asc or desc")]
        public string SortDirection { get; set; }

        public string SortBy { get; set; } = "Id";
    }
}