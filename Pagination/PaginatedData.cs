namespace EmployeeWebApi.Pagination
{
     public class PaginatedData<T>
    {
        public T Data { get; set; }

        public MetaData Meta { get; set; }
    }
}