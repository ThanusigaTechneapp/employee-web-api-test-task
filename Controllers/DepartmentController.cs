using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EmployeeWebApi.Models;
using EmployeeWebApi.Pagination;
using EmployeeWebApi.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EmployeeWebApi.Controllers
{
    [Route("api/[Controller]")]
    [ApiController]
    public class DepartmentController : ControllerBase
    {
        private readonly EmployeeDbContext _context;
        private readonly IDepartmentService _deparmentService;

        public DepartmentController(EmployeeDbContext contex, IDepartmentService deparmentService)
        {
            _context = contex;
            _deparmentService = deparmentService;
        }

        //GET: api/Department
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Department>>> GetAllDepartments([FromQuery] PaginatedQuery paginatedQuery)
        {

            var department = await _deparmentService.GetAllDepartments(paginatedQuery);

            return Ok(department);
        }

        // //GET: api/Department
        // [HttpGet]
        // public async Task<ActionResult<IEnumerable<Department>>> GetAllDepartments()
        // {

        //     var department = await _context.Thanusiga_Departments.ToListAsync();

        //     return department;
        // }

        //GET: api/Department/3
        [HttpGet("{id}")]
        public async Task<ActionResult<Department>> GetDepartment(int id)
        {
            var department = await _context.Thanusiga_Departments.FindAsync(id);

            if (department == null)
            {
                return NotFound();
            }

            return department;
        }

        //POST: api/Department
        [HttpPost]
        public async Task<ActionResult<Department>> PostDepartment(Department department)
        {
            _context.Thanusiga_Departments.Add(department);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetDepartment), new { id = department.Id }, department);

        }

        //PUT: api/Department/3
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDepartment(int id, Department department)
        {
            if (id != department.Id)
            {
                return BadRequest();
            }

            _context.Entry(department).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DepartmentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return Ok(new { message = "Department Updated" });
        }

        private bool DepartmentExists(int id)
        {
            throw new NotImplementedException();
        }

        //DELETE: api/Department/3
        [HttpDelete("{id}")]
        public async Task<ActionResult<Department>> DeleteDepartment(int id)
        {
            var department = await _context.Thanusiga_Departments.FindAsync(id);
            if (department == null)
            {
                return NotFound();
            }
            _context.Thanusiga_Departments.Remove(department);
            await _context.SaveChangesAsync();

            return Ok(new { message = "Department deleted" });
        }
    }
}