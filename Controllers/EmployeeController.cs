using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using EmployeeWebApi.Models;
using EmployeeWebApi.Pagination;
using EmployeeWebApi.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting.Internal;

namespace EmployeeWebApi.Controllers
{
    [Route("api/[Controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly EmployeeDbContext _context;
        private static IWebHostEnvironment _webHostEnvironment;
        private readonly IEmployeeService _employeeService;
        public EmployeeController(EmployeeDbContext context, IWebHostEnvironment webHostEnvironment, IEmployeeService employeeService)
        {
            _context = context;
            _webHostEnvironment = webHostEnvironment;
            _employeeService = employeeService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Employee>>> GetAllEmployees([FromQuery] PaginatedQuery paginatedQuery)
        {

            var employee = await _employeeService.GetAllEmployees(paginatedQuery);

            return Ok(employee);
        }

        //GET: api/Emplyeee
        // [HttpGet]
        // public async  Task<ActionResult<IEnumerable<Employee>>> GetAllEmployees(){

        //     var employee = await _context.Thanusiga_Employees.ToListAsync();

        //     return employee;
        // }


        //GET: api/Employee/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Employee>> GetEmployee(int id)
        {
            var employee = await _context.Thanusiga_Employees.FindAsync(id);

            if (employee == null)
            {
                return NotFound();
            }

            return Ok(employee);
        }

        //POST: api/Employee
        [HttpPost]
        public async Task<ActionResult<Employee>> PostEmployee([FromForm] Employee employee)
        {
            employee.ImageName = await UploadImage(employee.ImageFile);
            _context.Thanusiga_Employees.Add(employee);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetEmployee), new { id = employee.Id }, employee);

        }

        //PUT: api/Employee/3
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEmployee(int id, Employee employee)
        {

            if (id != employee.Id)
            {
                return BadRequest();
            }

            _context.Entry(employee).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmployeeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return Ok(new { message = "Employee updated" });
        }

        private bool EmployeeExists(int id)
        {
            throw new NotImplementedException();
        }


        //DELETE: api/Employee/3
        [HttpDelete("{id}")]
        public async Task<ActionResult<Employee>> DeleteEmployee(int id)
        {
            var employee = await _context.Thanusiga_Employees.FindAsync(id);
            if (employee == null)
            {
                return NotFound();
            }
            DeleteImage(employee.ImageName);
            _context.Thanusiga_Employees.Remove(employee);
            await _context.SaveChangesAsync();

            return Ok(new { message = "Employee deleted" });
        }

        [NonAction]
        public async Task<string> UploadImage(IFormFile imageFile)
        {
            // string imageName = new String(Path.GetFileNameWithoutExtension(imageFile.FileName).Take(10).ToArray()).Replace('' , '-');
            string imageName = imageFile.FileName;
            imageName = imageName + DateTime.Now.ToString("yymmssfff") + Path.GetExtension(imageFile.FileName);
            var imagePath = Path.Combine(_webHostEnvironment.ContentRootPath, "Images", imageName);
            using (var filestream = new FileStream(imagePath, FileMode.Create))
            {
                await imageFile.CopyToAsync(filestream);
            }
            return imageName;
        }

        private void DeleteImage(string url)
        {
            string path = url;
            FileInfo file = new FileInfo(path);
            if (file.Exists)//check file exsit or not  
            {
                file.Delete();
            }
        }


        [Route("GetAllDepartmentNames")]
        public async Task<List<Department>> GetAllGetAllDepartmentNames()
        {
            //     var depList = await _context.Thanusiga_Employees.Select(x => new Employee(){
            //         Department = x.Department
            //     }).ToListAsync();
            var depList = await _context.Thanusiga_Departments.Select(x => new Department()
            {
                Id = x.Id,
                Name = x.Name
            }).ToListAsync();

            return depList;

        }





    }

}