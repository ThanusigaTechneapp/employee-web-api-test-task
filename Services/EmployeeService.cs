using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmployeeWebApi.Models;
using EmployeeWebApi.Pagination;

namespace EmployeeWebApi.Services
{
    public class EmployeeService : IEmployeeService
    {

        private readonly EmployeeDbContext _context;
        public EmployeeService(EmployeeDbContext contex)
        {
            _context = contex;
        }
        public async Task<PaginatedData<List<Employee>>> GetAllEmployees(PaginatedQuery paginatedQuery)
        {
            var query = _context.Thanusiga_Employees.AsQueryable();

            if (!String.IsNullOrWhiteSpace(paginatedQuery.Keyword))
            {
                var keyword = paginatedQuery.Keyword;
                query = query.Where(d => d.Name.Contains(keyword) ||
                 d.Name.Contains(keyword.ToUpper()) ||
                 d.Name.Contains(keyword.ToLower()) || d.Name.StartsWith(keyword.ToLower()));

                //query = query.Where(h => EF.Functions.ILike(h.Id.ToString(), $"%{keyword}%"));
            }

            if (paginatedQuery.SortDirection == "asc")
            {
                query = query.OrderBy(h => h.Id);
            }
            else
            {
                query = query.OrderByDescending(h => h.Id);
            }

            var results = await PaginatedList<Employee>.CreateAsync(query, paginatedQuery.Page, paginatedQuery.PerPage);

            return new PaginatedData<List<Employee>>()
            {
                Data = results,
                Meta = new MetaData()
                {
                    TotalPages = results.TotalPages,
                    CurrentPage = results.PageIndex,
                    TotalCount = results.TotalCount,
                }
            };

        }


    }
}