using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmployeeWebApi.Models;
using EmployeeWebApi.Pagination;
using Microsoft.EntityFrameworkCore;

namespace EmployeeWebApi.Services
{
    public class DepartmentService : IDepartmentService
    {

        private readonly EmployeeDbContext _context;
        public DepartmentService(EmployeeDbContext contex)
        {
            _context = contex;
        }
        public async Task<PaginatedData<List<Department>>> GetAllDepartments(PaginatedQuery paginatedQuery)
        {
            var query = _context.Thanusiga_Departments.AsQueryable();

            if (!String.IsNullOrWhiteSpace(paginatedQuery.Keyword))
            {
                var keyword = paginatedQuery.Keyword;
                query = query.Where(d => d.Name.Contains(keyword) ||
                 d.Name.Contains(keyword.ToUpper()) ||
                 d.Name.Contains(keyword.ToLower()) || d.Name.StartsWith(keyword.ToLower()));

                //query = query.Where(h => EF.Functions.ILike(h.Id.ToString(), $"%{keyword}%"));
            }

            if (paginatedQuery.SortDirection == "asc")
            {
                query = query.OrderBy(h => h.Id);
            }
            else
            {
                query = query.OrderByDescending(h => h.Id);
            }

            var results = await PaginatedList<Department>.CreateAsync(query, paginatedQuery.Page, paginatedQuery.PerPage);

            return new PaginatedData<List<Department>>()
            {
                Data = results,
                Meta = new MetaData()
                {
                    TotalPages = results.TotalPages,
                    CurrentPage = results.PageIndex,
                    TotalCount = results.TotalCount,
                }
            };

        }


    }
}