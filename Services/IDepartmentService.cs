using System.Collections.Generic;
using System.Threading.Tasks;
using EmployeeWebApi.Models;
using EmployeeWebApi.Pagination;

namespace EmployeeWebApi.Services
{
    public interface IDepartmentService
    {

           Task<PaginatedData<List<Department>>> GetAllDepartments(PaginatedQuery paginatedQuery);

    }
}