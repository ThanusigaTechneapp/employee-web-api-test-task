using System.Collections.Generic;
using System.Threading.Tasks;
using EmployeeWebApi.Models;
using EmployeeWebApi.Pagination;

namespace EmployeeWebApi.Services
{
    public interface IEmployeeService
    {
        Task<PaginatedData<List<Employee>>> GetAllEmployees(PaginatedQuery paginatedQuery);

    }
}