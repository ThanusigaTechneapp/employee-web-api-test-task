﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EmployeeWebApi.Migrations
{
    public partial class departChanges1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Thanusiga_Employees_Thanusiga_Departments_DepartmentId",
                table: "Thanusiga_Employees");

            migrationBuilder.DropIndex(
                name: "IX_Thanusiga_Employees_DepartmentId",
                table: "Thanusiga_Employees");

            migrationBuilder.DropColumn(
                name: "DepartmentId",
                table: "Thanusiga_Employees");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DepartmentId",
                table: "Thanusiga_Employees",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Thanusiga_Employees_DepartmentId",
                table: "Thanusiga_Employees",
                column: "DepartmentId");

            migrationBuilder.AddForeignKey(
                name: "FK_Thanusiga_Employees_Thanusiga_Departments_DepartmentId",
                table: "Thanusiga_Employees",
                column: "DepartmentId",
                principalTable: "Thanusiga_Departments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
