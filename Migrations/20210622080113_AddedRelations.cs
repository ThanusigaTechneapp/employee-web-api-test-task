﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EmployeeWebApi.Migrations
{
    public partial class AddedRelations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Department",
                table: "Thanusiga_Employees");

            migrationBuilder.AddColumn<int>(
                name: "DepartmentId",
                table: "Thanusiga_Employees",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Thanusiga_Employees_DepartmentId",
                table: "Thanusiga_Employees",
                column: "DepartmentId");

            migrationBuilder.AddForeignKey(
                name: "FK_Thanusiga_Employees_Thanusiga_Departments_DepartmentId",
                table: "Thanusiga_Employees",
                column: "DepartmentId",
                principalTable: "Thanusiga_Departments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Thanusiga_Employees_Thanusiga_Departments_DepartmentId",
                table: "Thanusiga_Employees");

            migrationBuilder.DropIndex(
                name: "IX_Thanusiga_Employees_DepartmentId",
                table: "Thanusiga_Employees");

            migrationBuilder.DropColumn(
                name: "DepartmentId",
                table: "Thanusiga_Employees");

            migrationBuilder.AddColumn<string>(
                name: "Department",
                table: "Thanusiga_Employees",
                type: "text",
                nullable: true);
        }
    }
}
