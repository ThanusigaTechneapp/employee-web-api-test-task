﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace EmployeeWebApi.Migrations
{
    public partial class connectToMyLap : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Thanusiga_Departments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "varchar(100)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Thanusiga_Departments", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Thanusiga_Employees",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "varchar(100)", nullable: true),
                    ImageName = table.Column<string>(type: "varchar(100)", nullable: true),
                    DepartmentId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Thanusiga_Employees", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Thanusiga_Employees_Thanusiga_Departments_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "Thanusiga_Departments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Thanusiga_Employees_DepartmentId",
                table: "Thanusiga_Employees",
                column: "DepartmentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Thanusiga_Employees");

            migrationBuilder.DropTable(
                name: "Thanusiga_Departments");
        }
    }
}
