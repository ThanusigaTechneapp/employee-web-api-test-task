﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EmployeeWebApi.Migrations
{
    public partial class departChanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Department",
                table: "Thanusiga_Employees",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Department",
                table: "Thanusiga_Employees");
        }
    }
}
